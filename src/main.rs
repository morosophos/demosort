mod demo;
mod extractors;

use demo::*;
use extractors::*;
use failure::Error;
use std::collections::HashMap;
use std::fs::File;
use std::io::{BufRead, BufReader, Cursor, Write};
use std::path::PathBuf;
use std::process::Command;
use structopt::StructOpt;
use tempdir::TempDir;

const DEFAULT_NAME_FMT: &str = "democut-{start}-{stop}.dem";
const DEFAULT_RECORD_MARGIN: f32 = 5.;
const SEVEN_ZIP: &str = "7za";
const SKIP_FAST_TIME: f32 = 50.;
const SKIP_SLOW_TIME: f32 = 5.;
const SKIP_FAST_RATE: u8 = 100;
const SKIP_SLOW_RATE: u8 = 10;
const DEFAULT_MAX_POS: u8 = 10;

#[derive(Debug, Clone, Copy)]
enum CuttingState {
    Initial,
    SkipFast,
    SkipSlow,
    PreStart,
    Running,
}

#[derive(StructOpt, Debug)]
#[structopt()]
struct Opt {
    #[structopt(long = "debug")]
    /// dumps list of packets from the source demo. Does not write any output
    debug: bool,
    #[structopt(long = "player-id-map")]
    /// path to a file with crypto_idfp -> player-id mapping. Useful for demo name formatting (see --output-name-fmt option)
    player_id_map: Option<PathBuf>,
    #[structopt(long = "start")]
    /// cut from here. If --cts-record is specified, then cut this many seconds before the record run starts
    start: Option<f32>,
    #[structopt(long = "stop")]
    /// cut to here. If --cts-record is specified, then cut this many seconds after the record ends
    stop: Option<f32>,
    #[structopt(long = "cts-record")]
    /// extract the best cts record from the demo
    cts_record: bool,
    #[structopt(long = "cts-max-pos")]
    /// do not extract cts records above this position
    cts_max_pos: Option<u8>,
    #[structopt(long = "source", parse(from_os_str))]
    /// source demo
    source_path: PathBuf,
    #[structopt(long = "output-path", parse(from_os_str))]
    /// save the resulting demo in the specified file
    output_path: Option<PathBuf>,
    #[structopt(long = "output-dir", parse(from_os_str))]
    /// save the resulting demo in the specified directory (may be pk3 file)
    output_dir: Option<PathBuf>,
    #[structopt(long = "output-name-fmt")]
    /// when --output-dir is used format the output file name according to the specified format
    output_name_fmt: Option<String>,
}

impl Opt {
    fn get_reader(&self) -> Result<PacketReader<File>, Error> {
        let f = File::open(&self.source_path)?;
        Ok(PacketReader::new(f))
    }

    fn get_player_id_map(&self) -> Result<HashMap<String, String>, Error> {
        let mut res = HashMap::new();
        if let Some(ref file_name) = self.player_id_map {
            let f = File::open(file_name)?;
            let bufreader = BufReader::new(f);
            for i in bufreader.lines() {
                let line = i?;
                let mut split = line.split_whitespace();
                let crypto_idfp = split.next();
                let readable_id = split.next();
                if let (Some(c), Some(r)) = (crypto_idfp, readable_id) {
                    res.insert(String::from(c), String::from(r));
                }
            }
        }
        Ok(res)
    }

    fn extract(
        &self,
        reader: &mut PacketReader<File>,
    ) -> Result<Option<Box<dyn Extracted>>, Error> {
        if self.cts_record {
            let mut extractor = CTSRecordExtractor {
                reader,
                start_margin: self.start.unwrap_or(DEFAULT_RECORD_MARGIN),
                stop_margin: self.stop.unwrap_or(DEFAULT_RECORD_MARGIN),
                max_pos: self.cts_max_pos.unwrap_or(DEFAULT_MAX_POS),
            };
            extractor.extract()
        } else {
            let mut extractor = SimpleExtractor {
                reader,
                start: self.start.unwrap_or(0.),
                stop: self.stop.unwrap_or(0.),
            };
            extractor.extract()
        }
    }

    fn write_output(&self, extracted: Box<dyn Extracted>, output: &[u8]) -> Result<(), Error> {
        if let Some(ref path) = self.output_path {
            let mut f = File::create(path)?;
            f.write_all(output)?;
        } else if let Some(ref dir) = self.output_dir {
            let output_name_fmt = self
                .output_name_fmt
                .as_ref()
                .map(String::as_str)
                .unwrap_or(DEFAULT_NAME_FMT);
            let id_map = self.get_player_id_map()?;
            let mut params = extracted.fmt_args();
            if let Some(crypto_idfp) = params.get("crypto_idfp") {
                if let Some(readable_id) = id_map.get(crypto_idfp) {
                    params.insert(String::from("player_id"), readable_id.clone());
                }
            }
            // TODO: better error reporting for failed formatting
            let file_name = strfmt::strfmt(output_name_fmt, &params)?;
            if dir.is_dir() {
                let mut f = File::create(dir.join(&file_name))?;
                f.write_all(output)?;
            } else if let Some(ext) = dir.extension() {
                if ext == "pk3" {
                    // I'd love to replace this shit with a good zip library
                    // But zip crate is beyond usability so far
                    // (slow and inefficient compression and doesn't support appending files to archives)
                    // using 7z
                    let temp = TempDir::new("demosort")?;
                    let demo_path = temp.path().join("demos");
                    let file_path = demo_path.join(&file_name);
                    std::fs::create_dir(&demo_path)?;
                    {
                        let mut f = File::create(&file_path)?;
                        f.write_all(output)?;
                    }
                    Command::new(SEVEN_ZIP)
                        .args(&[
                            "a",
                            "-tzip",
                            "-mx=9",
                            &dir.to_string_lossy(),
                            &demo_path.to_string_lossy(),
                        ])
                        .status()?;
                }
            }
        } else {
            println!("You must specify either --output-path or --output-dir\nDoing nothing")
        }
        Ok(())
    }
}

fn main() -> Result<(), Error> {
    let opt = Opt::from_args();
    let mut reader = opt.get_reader()?;
    if opt.debug {
        reader.read_cdtrack()?;
        for packet in reader {
            let p = packet.expect("Failed to parse packet");
            println!("{:?}", p.packet);
        }
    } else {
        let extracted = match opt.extract(&mut reader)? {
            Some(e) => e,
            None => {
                println!("Could not extract requested piece from the demo");
                return Ok(());
            }
        };

        reader.reset()?;
        let cdtrack = reader.read_cdtrack()?;
        let mut state = CuttingState::Initial;
        let mut out = Cursor::new(Vec::new());
        out.write_all(&cdtrack)?;
        out.write_all(&[b'\n'])?;
        let start = extracted.start();
        let stop = extracted.stop();
        for packet in reader {
            let p = packet.expect("Failed to parse packet");
            if let DemoPacketContents::ServerPacket(SVC::Time(t)) = p.packet {
                match state {
                    CuttingState::Initial => {
                        state = CuttingState::SkipFast;
                        DemoPacket::make_slowmo(SKIP_FAST_RATE).write(&mut out)?;
                    }
                    CuttingState::SkipFast => {
                        if t > start - SKIP_FAST_TIME {
                            state = CuttingState::SkipSlow;
                            DemoPacket::make_slowmo(SKIP_SLOW_RATE).write(&mut out)?;
                        }
                    }
                    CuttingState::SkipSlow => {
                        if t > start - SKIP_SLOW_TIME {
                            state = CuttingState::PreStart;
                            DemoPacket::make_slowmo(1).write(&mut out)?;
                        }
                    }
                    CuttingState::PreStart => {
                        if t > start {
                            state = CuttingState::Running;
                            DemoPacket::make_videostart().write(&mut out)?;
                        }
                    }
                    CuttingState::Running => {
                        if t > stop {
                            DemoPacket::make_videostop().write(&mut out)?;
                            DemoPacket::make_disconnect().write(&mut out)?;
                            break;
                        }
                    }
                }
            }
            p.write(&mut out)?;
        }
        opt.write_output(extracted, &out.into_inner())?;
    }

    Ok(())
}

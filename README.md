# demosort

A small utility to insert playback speed adjustment and video recording commands into [Xonotic](https://xonotic.org/) demos.

## why can it be useful

Mostly for two things:

 * it automates fast-forwarding to the interesting piece of demo
 * it automates recording video of that piece
 
## How does it know which piece of demo is interesting?

There are two ways:

 * user specifies start and stop timestamps of the interesting piece
 * the utility finds the best CTS record in the demo

## why not demotc.pl + demotc-race-record-extractor.sh

Few reasons:

 * they generate two output demos (one for playback, one for video recording), which is waste of space
 * I modified Xonotic's CTS record markers to include additional data, which demotc.pl doesn't support
 * You can further optimize the output size by removing all demo data after the recorded piece ends (unfortunately it doesn't seem possible to skip the data before the recorded piece starts)
 * demotc.pl written in Perl so I'm reluctant to modify it :)
 
## what additional features does it have?

 * it's able to save the output into a .pk3 file (unfortunately you need p7zip for it cause the only rust's zip library sucks)
 * it allows to format output file name using the metadata of the CTS record found

## how to use it?

It has nice embedded CLI doc (`./demosort --help`)

```
USAGE:
    demosort [FLAGS] [OPTIONS] --source <source_path>

FLAGS:
        --cts-record    extract the best cts record from the demo
        --debug         dumps list of packets from the source demo. Does not write any output
    -h, --help          Prints help information
    -V, --version       Prints version information

OPTIONS:
        --cts-max-pos <cts_max_pos>            do not extract cts records above this position
        --output-dir <output_dir>              save the resulting demo in the specified directory (may be pk3 file)
        --output-name-fmt <output_name_fmt>    when --output-dir is used format the output file name according to the
                                               specified format
        --output-path <output_path>            save the resulting demo in the specified file
        --player-id-map <player_id_map>        path to a file with crypto_idfp -> player-id mapping. Useful for demo
                                               name formatting (see --output-name-fmt option)
        --source <source_path>                 source demo
        --start <start>                        cut from here. If --cts-record is specified, then cut this many seconds
                                               before the record run starts
        --stop <stop>                          cut to here. If --cts-record is specified, then cut this many seconds
                                               after the record ends

```

`--output-name-fmt` uses rust-like syntax for string substitution. For example:

`--output-name-fmt cts-recocrd-by-{player_id}-{pos}-{record_f}.dem`

Variables available for `--output-name-fmt`:

 * start
 * stop
 * pos (CTS record position)
 * record (CTS record time in human-readable form)
 * record_f (CTS record time as float number)
 * crypto_idfp (crypto_idfp of record holder)
 * player_id (player_id found in the mapping specified by `--player-id-map`)
 
## Example CLI invocation

```
$ ./demosort --source nice_demo.dem --player-id-map id_map.txt --cts-record --cts-max-pos 5 --output-dir nice-demos.pk3 --output-name-fmt "nice-demo-{player_id}-{pos}-{record_f}.dem"
```

## note about video recording

Unlike demotc.pl which toggles `cl_capturevideo` cvar directly, demosort calls `startvideo` and `stopvideo` commands. By default xonotic has no such commands, so the demo won't trigger video recording and will be working as a pure playback demo.

To start video recording upon reaching the specified demo piece define the following aliases in your Xonotic config:

```
alias startvideo "cl_capturevideo 1"
alias stopvideo "cl_capturevideo 0"
```

Such workaround allows to not have separate playback and video demos like demotc-race-record-extractor.sh does.

## missing features and bugs

 * requires external utility to write into .pk3 files (p7zip)
 * would be useful to have code to cut out warm-up from demos of competitive games
